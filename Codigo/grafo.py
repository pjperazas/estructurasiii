from collections import defaultdict
import random
import cv2

# Se crea una clase grafo  
class Grafo():
    def __init__(self):
        self.nodos = []
        self.aristas = defaultdict(list)
        self.pesos = {}
        self.aristas_temp = []
        self.nodos_cmp = []
        
        
    def añadir_arista(self, origen, destino, peso):
        # Asume que es bidireccional  
        self.aristas[origen].append(destino)
        self.aristas[destino].append(origen)
        self.pesos[(origen, destino)] = peso
        self.pesos[(destino, origen)] = peso

        # Funcion que busca el camino más corto, en esta caso usamos dijsktra
    def dijsktra(self, inicial, final):
        # caminos_mas_cortos es un dict de nodos
        # cuyos valores son tuples con:( nodo_anterior, peso)
        caminos_mas_cortos = {inicial: (None, 0)}
        nodo_actual = inicial
        visitado = set()
        
        # Busca el camino mas corto hasta llegar al final
        while nodo_actual != final:
            visitado.add(nodo_actual)
            destinos = self.aristas[nodo_actual]
            peso_a_nodo_actual = caminos_mas_cortos[nodo_actual][1]

            for nodo_sig in destinos:
                peso = self.pesos[(nodo_actual, nodo_sig)] + peso_a_nodo_actual
                if nodo_sig not in caminos_mas_cortos:
                    caminos_mas_cortos[nodo_sig] = (nodo_actual,peso )
                else:
                    peso_mas_pequño_acutal = caminos_mas_cortos[nodo_sig][1]
                    if peso_mas_pequño_acutal > peso:
                        caminos_mas_cortos[nodo_sig] = (nodo_actual, peso)
            
            destinos_sig = {nodo: caminos_mas_cortos[nodo] for nodo in caminos_mas_cortos if nodo not in visitado}
            if not destinos_sig:
                return "No hay un camino disponible"
            # nodo es el destino con menos peso
            
            nodo_actual = min(destinos_sig, key=lambda k: destinos_sig[k][1])
        
        # nos devolvemos a destino por el camino mas corto 
        camino = []
        while nodo_actual is not None:
            camino.append(nodo_actual)
            nodo_sig = caminos_mas_cortos[nodo_actual][0]
            nodo_actual = nodo_sig
            
        # se le da vuelta a camino 
        camino = camino[::-1]
        return camino


    # Funcion que añade las aritas a 3 lista:
    #aristas_temp para el dijsktra,
    #nodos_cmp para verificar si ya estan añadia una arista,
    #aristas que añade lis pesos
    def añadir_aristas(self):
        x = random.randint(1, 99)
        cont = 0    
        for nodo1 in self.nodos:
                for nodo2 in self.nodos:
                        if nodo1 == nodo2:
                                continue
                        elif ((nodo1,nodo2)) not in self.nodos_cmp and ((nodo2,nodo1)) not in self.nodos_cmp:
                                if nodo1.forma == nodo2.forma or nodo1.color == nodo2.color:
                                        self.aristas_temp += [(nodo1.id,nodo2.id)]
                                        self.aristas_temp += [(nodo2.id,nodo1.id)]
                                        
                                        self.nodos_cmp += [(nodo1,nodo2)]
                                        self.nodos_cmp += [(nodo2,nodo1)]

  

        for arista in self.aristas_temp:
            x = random.randint(1, 99)
            cont = 0
            self.añadir_arista((*arista),x)
            if cont == 2:
                cont=0
                x = random.randint(1, 99)
            cont = cont+1


    # Imprime la lista de adyacencia 
    def imprimir(self):
        print("lista de Adyacencia:")
        for nodo in self.nodos:
                print(nodo.id,":", end='')
                lista_adyacentes = [] 
                for edge in self.aristas_temp:
                
                        if nodo.id == edge[0]:
                                lista_adyacentes += [edge[1]]
                if lista_adyacentes != []:               
                    print(lista_adyacentes)
                else:
                    print("[]")
                    
        print('')
         # Imprime los nodos y con sus respectivos enlaces 
        print("enlaces de cada nodo con su respectivo peso:")
        for nodo in self.nodos:
                lista_adyacentes = []
                print(nodo.id,":", end='')
                for edge in self.aristas_temp:
                        if nodo.id == edge[0]:
                                lista_adyacentes += [(edge[1],self.pesos[(edge)])]
                                
                cont = 0
                if lista_adyacentes != []:
                    while cont != len(lista_adyacentes):
                            print(lista_adyacentes[cont])
                            cont = cont +1
                    print('')
                else:print("()")
                
         #Imprime el camino que retorna el dijsktra y el peso final              
    def imprime_dijsktra(self,camino):
        cont = 0
        peso = 0
        if camino == "No hay un camino disponible":
            print(camino)
        else:
            while cont != len(camino):
                
                if cont < len(camino)-1:
                    print(camino[cont],"->",end=' ')
                    peso = peso + self.pesos[(camino[cont],camino[cont+1])]
                    cont = cont+1
                else:
                    print(camino[cont], end=' ')
                    cont = cont+1
            print("con un peso total de: ",peso)


            
            
    #realiza el algoritmo de prim utilizando tres listas distintas, retorna los nodos recorridos y luego los posibles caminos
    #siendo el primero o el unico el que el algoritmo elije  
    def prim(self,nodo_inicio):
        
        nodos_visitados = []
        grafoResultante = {}
        listaOrdenada = []


        #se agrega el nodo de inico a la lista de visitados
        nodos_visitados.append(nodo_inicio.id)

        #se agregan los posibles caminos a la lista ordenada por pesos de menor a mayor
        for edge in self.aristas_temp:
            if nodo_inicio.id == edge[0]:
                listaOrdenada.append((edge[0],edge[1],self.pesos[(edge)]))
                
        listaOrdenada = [(c,a,b) for a,b,c in listaOrdenada]
        listaOrdenada.sort()
        listaOrdenada = [(a,b,c) for c,a,b in listaOrdenada]
         
            
        while listaOrdenada:
          #elimina el nodo de la lista con los posibles caminos
          nodo = listaOrdenada.pop(0)
          des = nodo[1] 


          #se busca un destino fuera de la lista de nodos visitados
          if des not in nodos_visitados:
            nodos_visitados.append(des)
            for edge in self.aristas_temp:
                if edge[0] ==des:
                    if edge[1]  not in nodos_visitados:
                        listaOrdenada.append((edge[0],edge[1],self.pesos[(edge)]))

                
	    #se ordena la lista con los posibles caminos, osea los nodos adyacentes esto mediante a su peso
            listaOrdenada = [(c,a,b) for a,b,c in listaOrdenada]
            listaOrdenada.sort()
            listaOrdenada = [(a,b,c) for c,a,b in listaOrdenada]
            #esto se agrega al grafo final que es el que se imprime
            nodo_inicio  = nodo[0]
            destino = nodo[1]
            peso    = nodo[2]

            if nodo_inicio in grafoResultante:
              if destino in grafoResultante:
                lista = grafoResultante[nodo_inicio]
                grafoResultante[nodo_inicio] = lista + [(destino, peso)]
                lista = grafoResultante[destino]
                lista.append((nodo_inicio, peso))
                grafoResultante[destino] = lista
              else:
                grafoResultante[destino] = [(nodo_inicio, peso)]
                lista = grafoResultante[nodo_inicio]
                lista.append((destino, peso))
                grafoResultante[nodo_inicio] = lista
            elif destino in grafoResultante:
              grafoResultante[nodo_inicio] = [(destino, peso)]
              lista = grafoResultante [destino]
              lista.append((nodo_inicio, peso))
              grafoResultante[destino] = lista
            else:
              grafoResultante[destino] = [(nodo_inicio, peso)]
              grafoResultante[nodo_inicio] = [(destino, peso)]         
                        


                        
        for key, lista in grafoResultante.items():
          if key == nodos_visitados[0]:
            print(key)
            print(lista)

        for key, lista in grafoResultante.items():
          if key != nodos_visitados[0]:
            print(key)
            print(lista) 


    # imprime las aristas en la imagen
    def imprimir_aristas(self, imagen):
        for nodo in self.nodos:
                for nodo2 in self.nodos_cmp:
                        if nodo == nodo2[0]:
                            image = cv2.line(imagen, nodo.posicion, nodo2[1].posicion, (251, 0, 255), 1)
                            cv2.imshow("Analizando...", image)
                            cv2.waitKey(2000)
                            
 
                                

                             
        
               
