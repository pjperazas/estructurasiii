# import the necessary packages
from shapedetector import ShapeDetector
from colorlabeler import ColorLabeler
import argparse
import imutils
import random 
from grafo import Grafo
from nodo import Nodo
import cv2

grafo = Grafo()
indice = 1


# Carga la imagen y la reajusta la ventana
imagen = cv2.imread('shapes_and_colors.jpg')
resized = imutils.resize(imagen, width=300)
ratio = imagen.shape[0] / float(resized.shape[0])
 
# Pasa la imagen a escala de grises, la desenfoca y las ajusta a un threshold
blurred = cv2.GaussianBlur(resized, (5, 5), 0)
gray = cv2.cvtColor(blurred, cv2.COLOR_BGR2GRAY)
lab = cv2.cvtColor(blurred, cv2.COLOR_BGR2LAB)
thresh = cv2.threshold(gray, 60, 255, cv2.THRESH_BINARY)[1]
 
# Encuentra los contornos e inicializa el shapeDetector
cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
	cv2.CHAIN_APPROX_SIMPLE)
cnts = imutils.grab_contours(cnts)
sd = ShapeDetector()
cl = ColorLabeler()

# Un loop que reconoce y guarda cada figura
for c in cnts:
	
        # Saca el centro , el nombre de la figura y el color
	M = cv2.moments(c)
	cX = int((M["m10"] / M["m00"]) * ratio)
	cY = int((M["m01"] / M["m00"]) * ratio)
	shape = sd.detect(c)
	color = cl.label(lab, c)
	
        # Se crea un nodo con las caracteristicas de la figura
	nodo = Nodo(indice, shape, color, [],cX,cY)
	grafo.nodos += [nodo]
	indice += 1

        #Multiplica el contorno y dibuja la figura la figura y escribe su id y sus caracteristicas
	c = c.astype("float")
	c *= ratio
	c = c.astype("int")
	cv2.drawContours(imagen, [c], -1, (0, 255, 0), 2)
	
	cv2.putText(imagen,str(indice - 1) + " " + shape + " " + color, (cX, cY), cv2.FONT_HERSHEY_SIMPLEX,
		0.5, (255, 255, 255), 2)
 
	# muestra una ventana con la imagen 
	cv2.imshow("Analizando...", imagen)
	cv2.waitKey(2000)
	

 

# Con los nodos guardados se generan las aristas 
grafo.añadir_aristas()

#Se imprimen las aristas en la imagen  
grafo.imprimir_aristas(imagen)

#Se imprimen la lista de adyacencia y cada nodo con sus enlaces  
grafo.imprimir()

inicio = 1
final = 6

print("El camino más corto entre ",inicio," y " ,final," sería:",end =' ')
# Se imprime la ruta mas corta entre 2 nodos 
grafo.imprime_dijsktra(grafo.dijsktra(inicio, final))


inicio_prim = grafo.nodos[0]
# Se imprime el recorrido prim
print("recorrido prim iniciando en: ",inicio_prim.id)
grafo.prim(inicio_prim)

# Input para que el usuario termine el programa
x = input("Enter para Finalizar ")
cv2.destroyAllWindows()
