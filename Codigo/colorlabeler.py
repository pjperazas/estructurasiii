from scipy.spatial import distance as dist
from collections import OrderedDict
import numpy as np
import cv2
 
class ColorLabeler:
	def __init__(self):
		# Inicializa el diccionario de colores.
		colors = OrderedDict({
			"rojo": (255, 0, 0),
			"verde": (0, 255, 0),
			"azul": (0, 0, 255),
                        "amarillo": (255, 255, 0),
                        "anaranjado": (255, 128, 0)})
 
		# Asignamos espacio en memoria para la nueva imagen L*a*b*.
		self.lab = np.zeros((len(colors), 1, 3), dtype="uint8")
		self.colorNames = []
 
		# Se actualiza la lista de nombres de colores.
		for (i, (name, rgb)) in enumerate(colors.items()):
			self.lab[i] = rgb
			self.colorNames.append(name)
 
		# Convertimos la imagen de RGB a L*a*b*.
		self.lab = cv2.cvtColor(self.lab, cv2.COLOR_RGB2LAB)

	def label(self, image, c):
		# Construimons una máscara para los contornos.
		mask = np.zeros(image.shape[:2], dtype="uint8")
		cv2.drawContours(mask, [c], -1, 255, -1)
		mask = cv2.erode(mask, None, iterations=2)
		mean = cv2.mean(image, mask=mask)[:3]
		minDist = (np.inf, None)
 
		for (i, row) in enumerate(self.lab):
			# Se computa la "distancia" entre el actual valor de color L*a*b* y el promedio de la imagen.
			d = dist.euclidean(row[0], mean)
 
			# Si la distacia es menor, entonces se actualiza.
			if d < minDist[0]:
				minDist = (d, i)
 
		# Retornamos el nombre del color con la menor distancia.
		return self.colorNames[minDist[1]]
