import cv2
 
class ShapeDetector:
	def __init__(self):
		pass
 
	def detect(self, c):
		# Inicializa el nombre de la figura y aproxima el contorno.
		shape = ""
		peri = cv2.arcLength(c, True)
		approx = cv2.approxPolyDP(c, 0.04 * peri, True)

                # Si la figura es un triángulo, tendrá tres vértices.
		if len(approx) == 3:
			shape = "triangulo"
 
		# Si la figura es un cuadrado, tendrá cuatro vértices.
		elif len(approx) == 4:
			# compute the bounding box of the contour and use the
			# bounding box to compute the aspect ratio
			(x, y, w, h) = cv2.boundingRect(approx)
			ar = w / float(h)
 
			# a square will have an aspect ratio that is approximately
			# equal to one, otherwise, the shape is a rectangle
			shape = "cuadrado" if ar >= 0.95 and ar <= 1.05 else "rectangulo"
 
		# Si la figura es un pentágono, tendrá cinco vértices.
		elif len(approx) == 5:
			shape = "pentagono"
 
		# De lo contrario, asumimos que la figura es un círculo.
		else:
			shape = "circulo"
 
		# Retorna el nombre de la figura.
		return shape
